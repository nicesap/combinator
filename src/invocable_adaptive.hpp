#pragma once

#include "invocable_adaptive.h"

namespace combinatorial
{
    template <typename State>
    InvocableAdaptive<State>::InvocableAdaptive
    (
        InvocableFamily<State> family
        , size_t historySize
    )
        : mFamily{std::move(family)}
        , mHistorySize{historySize}
        , mCounters(historySize, 0)
        , mPermutation(historySize, 0)
    {
        std::iota(mPermutation.begin(), mPermutation.end(), 0);
    }

    template <typename State>
    void InvocableAdaptive<State>::invoke(State& currentState)
    {
        // TODO add concepts
        // ADL
        auto initialScore = score(currentState);
        State initialState = currentState;

        updatePermutation();
        loadOutdatedCounters();

        for (size_t index : mPermutation)
        {
            const Invocable<State>& instance = mFamily[index];

            instance(currentState);

            auto currentScore = score(currentState);

            if (currentScore > initialScore)
            {
                currentState = initialState;
                failure(index);
            }
            else if (currentScore < initialScore)
            {
                success(index);
                return;
            }
        }
    }

    template <typename State>
    void InvocableAdaptive<State>::updatePermutation()
    {
        std::sort
        (
              mPermutation.begin()
            , mPermutation.end()
            , [this](const size_t i1, const size_t i2)
            {
                return mCounters[i1] > mCounters[i2];
            }
        );
    }

    template <typename State>
    void InvocableAdaptive<State>::loadOutdatedCounters()
    {
    }

    template <typename State>
    void InvocableAdaptive<State>::success(const size_t index)
    {
        mCounters[index]++;
        //history
    }
}
