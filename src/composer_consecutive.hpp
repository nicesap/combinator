#pragma once

#include "composer_consecutive.h"

namespace combinatorial
{
    template <typename State>
    ComposerConsecutive<State>::ComposerConsecutive
    (
        InvocableSequence<State> sequence
    )
        : mSequence{std::move(sequence)}
    {
    }

    template <typename State>
    void ComposerConsecutive<State>::invoke(State& state)
    {
        for (Invocable<State>& invocable : mSequence)
        {
            invocable(state);
        }
    }
}
