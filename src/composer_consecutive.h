#pragma once

#include "invocable_family.h"

namespace combinatorial
{
    template <typename State>
    class ComposerConsecutive final : public InvocableI<State>
    {
    public:
        explicit ComposerConsecutive
        (
            InvocableSequence<State> sequence
        );

        void invoke(State& state) override;

    private:
        InvocableSequence<State> mSequence;
    };
}

#include "composer_consecutive.hpp"
