#pragma once

#include "invocable_i.h"

#include <vector>

namespace combinatorial
{
    template <typename T>
    using Sequence = std::vector<T>;

    template <typename State>
    using InvocableFamily = std::vector<Invocable<State>>;

    template <typename State>
    using InvocableSequence = Sequence<Invocable<State>>;
}
