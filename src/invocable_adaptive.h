#pragma once

#include "invocable_family.h"

#include <boost/circular_buffer.hpp>
#include <boost/dynamic_bitset.hpp>

namespace combinatorial
{
    template <typename State>
    class InvocableAdaptive final : public InvocableI<State>
    {
    public:
        explicit InvocableAdaptive
        (
            InvocableFamily<State> family
            , size_t historySize
        );

        void invoke(State& state) override;

    private:
        void updatePermutation();
        void loadOutdatedCounters();
        void failure(size_t index);
        void success(size_t index);

        InvocableFamily<State> mFamily;
        size_t mHistorySize;

        boost::circular_buffer<boost::dynamic_bitset<>> mHistory;
        std::vector<size_t> mCounters;
        std::vector<size_t> mPermutation;
    };
}

#include "invocable_adaptive.hpp"
