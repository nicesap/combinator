#pragma once

#include <memory>
#include <functional>

namespace combinatorial
{
    template <typename State>
    class InvocableI
    {
    public:
        virtual ~InvocableI() = default;
        virtual void invoke(State& state) = 0;
    };

    template <typename State>
    using Invocable = std::function<void(State&)>;

    template <typename T, typename State>
    concept ImplementsInvocableI = std::is_base_of_v<InvocableI<State>, T>;

    template <typename State>
    Invocable<State> invocableFromI(std::shared_ptr<InvocableI<State>> impl)
    {
        return [impl = std::move(impl)](State& state) mutable
        {
            impl->invoke(state);
        };
    }

    template <typename State, ImplementsInvocableI<State> Impl, typename... Args>
    Invocable<State> makeInvocable(Args&&... args)
    {
        return invocableFromI<State>
        (
            std::make_shared<Impl>
            (
                std::forward<Args>(args)...
            )
        );
    }
}
