#pragma once

#include "composer_bruteforce.h"

#include <deque>
#include <utility>

namespace combinatorial
{
    template <typename State>
    ComposerBruteForce<State>::ComposerBruteForce
    (
        Sequence<InvocableFamily<State>> sequenceOfAlternatives
    )
        : mSequenceOfAlternatives{std::move(sequenceOfAlternatives)}
    {
    }

    template <typename State>
    void ComposerBruteForce<State>::invoke(State& bestState)
    {
        // TODO add concepts
        // ADL
        auto bestScore = score(bestState);

        std::deque<std::pair<size_t, State>> deque{{0, bestState}};

        while (not deque.empty())
        {
            const auto [i, s] = deque.front();

            deque.pop_front();

            for (const auto& invocable : mSequenceOfAlternatives[i])
            {
                State currentState = s;
                invocable(currentState);
                auto currentScore = score(currentState);

                if (currentScore < bestScore)
                {
                    bestScore = std::move(currentScore);
                    bestState = currentState;
                }

                if (i + 1 < mSequenceOfAlternatives.size())
                {
                    deque.emplace_back(i + 1, std::move(currentState));
                }
            }
        }
    }
}
