#pragma once

#include "composer_hill_climbing.h"

namespace combinatorial
{
    template <typename State>
    ComposerHillClimbing<State>::ComposerHillClimbing
    (
        InvocableFamily<State> family
    )
        : mFamily{std::move(family)}
    {
    }

    template <typename State>
    void ComposerHillClimbing<State>::invoke(State& currentState)
    {
        // TODO add concepts
        // ADL
        auto bestScore = score(currentState);
        State bestState = currentState;

        bool shouldStop = false;
        while (not shouldStop)
        {
            shouldStop = true;

            for (Invocable<State>& instance : mFamily)
            {
                instance(currentState);

                auto currentScore = score(currentState);

                if (currentScore > bestScore)
                {
                    currentState = bestState;
                }
                else if (currentScore < bestScore)
                {
                    bestScore = std::move(currentScore);
                    bestState = currentState;
                    shouldStop = false;
                }
            }
        }

        currentState = std::move(bestState);
    }
}
