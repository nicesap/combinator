#pragma once

#include "invocable_family.h"

namespace combinatorial
{
    template <typename State>
    class ComposerHillClimbing final : public InvocableI<State>
    {
    public:
        explicit ComposerHillClimbing
        (
            InvocableFamily<State> family
        );

        void invoke(State& state) override;

    private:
        InvocableFamily<State> mFamily;
    };
}

#include "composer_hill_climbing.hpp"
