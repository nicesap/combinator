#pragma once

#include "invocable_family.h"

namespace combinatorial
{
    template <typename State>
    class ComposerBruteForce final : public InvocableI<State>
    {
    public:
        explicit ComposerBruteForce
        (
            Sequence<InvocableFamily<State>> sequenceOfAlternatives
        );

        void invoke(State& state) override;

    private:
        Sequence<InvocableFamily<State>> mSequenceOfAlternatives;
    };
}

#include "composer_bruteforce.hpp"
